<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link href="font/css/open-iconic-bootstrap.css" rel="stylesheet">
	  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	  <script type="text/javascript">
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {
      var data = new google.visualization.DataTable();
	
	  <?php

include('LIB_mysql.php');
$e=array();
$sql="SELECT DISTINCT city from covid.records WHERE NOT city=\"All of Orange County\"";
$cities=exe_sql("covid",$sql);
echo "data.addColumn('date', 'date');";
//$lastcity=array_pop($cities);
foreach($cities as $c=>$city){
  $cityarray[]=$city['city'];
	echo "data.addColumn('number', '$city[city]');";
}
$sql2="SELECT DISTINCT date from records";
$dates=exe_sql("covid",$sql2);
echo "data.addRows([";
$n=count($dates);
foreach($dates as $c=>$date){
	//$jsdate=
$djs=date("Y,m,d,0",strtotime($date['date']."-1 month"));
$d=date("Y,m,d,0",strtotime($date['date']));
	$sql3="SELECT city,count from records where  NOT city=\"All of Orange County\" AND DATE(date)=\"$d\"";
	$counts=exe_sql("covid",$sql3);
	//  $lastcities=array_pop($counts);
	echo "[new Date($djs),";
	foreach($counts as $k=>$cs){
   // $cityarray;
    $ctonly[]=$cs['count'];
  //  $countarray[$cs['city']]=$cs['count'];	
  }
  echo implode(',',$ctonly);
  $ctonly='';
  echo "]";
  if (($c+1) != $n)echo ",";
}
echo "]);";

?>
     
   

    

      var options = {
		  'title':'Orange County total COVID Cases by city per day',
		  'width':'100%',
		  'height':500,
        hAxis: {
          title: 'Date'
        },
        vAxis: {
          title: 'Total Case Count'
        }
		  
      //  series: {
      //    1: {curveType: 'function'}
      //  }
      };
var formatter_medium = new google.visualization.DateFormat({formatType: 'medium'});
formatter_medium.format(data,0);
  var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }
	</script>
	
	
	<style>
	body {
  background-image: url("/images/nicknguyen.combackground.fw.png");
    background-repeat: no-repeat;
  background-position: right bottom;
}
	</style>
	
	
	
    </head>
  <body>

   
   
   
<div class="container-fluid">
 <div class="row"><h1 class='display-3'>OC COVID Progression per city</h1></div>
 <div><p class="lead">This is my personal project to track the count of new positive COVID cases by day, per city in Orange County CA.  Data is mined from <a href="https://occovid19.ochealthinfo.com/coronavirus-in-oc">OC Health Agency</a> each day at 5pm PST. Why? Cause the world is on lock down, I <3 Data mining, and I have nothing better to do.</p></div>
  <div class="row"><div id="chart_div" style="width:100%"></div> </div>
  <div class="row"><p class="lead"> If your afraid of getting infected by the COVID-19 virus, you'll probably want to avoid these 3 cities while in Orange County 
  <?php
  $sqltop="SELECT city,count FROM covid.records WHERE NOT city=\"All of Orange County\" ORDER BY count DESC limit 20;";
  $tc=exe_sql("covid",$sqltop);
  foreach($tc as $s=>$r){
	  if(!isset($first)){$first=$r[city]; echo "<strong>".$first."</strong> with <strong>".$r[count]."</strong> ,";}
	  if($first!=$r[city] && $second!=$r[city]  && !isset($second)){$second=$r[city]; echo "<strong>".$second."</strong> with <strong> $r[count] </strong> , and ";}
	    if($first!=$r[city] && $second!=$r[city] && $third!=$r[city] && !isset($third)){$third=$r[city]; echo "<strong>".$third."</strong> with <strong>$r[count]</strong>";}
	  
  }

  ?>
  cases.</p></div> <div class="row"> <p>Other data points worth considering (data aquired from CDC.gov). <a href="https://wonder.cdc.gov/controller/datarequest/D76;jsessionid=1E0DC50B018B0C255044DB01D6AF9F0A">2018 Influenza and pneumonia Deaths</a>: 564 thats an average of 47 each month, As of 4/18 there have been 32 deaths. </p><p>It will be interesting to see if this rate increases or decreases in the future, because the likelihood of misclassification due to similar conditions </p></div><div class="row"><h2> Daily Color Indicator per city</h2><span style="display:inline-block; margin:5px; padding:3px;" class="badge badge-danger text-white text-center align-middle"> Over 3 new cases</span><span style="display:inline-block; margin:5px; padding:3px;" class="badge badge-warning  text-white text-center align-middle"> 2 New Cases </span><span style="display:inline-block; margin:5px; padding:3px;" class="badge badge-info  text-white text-center align-middle">1 New Case </span><span style="display:inline-block; margin:5px; padding:3px;" class="badge badge-success  text-white text-center align-middle">0 New Cases</span></div>
 <div class="row"><?php

//include('lib/LIB_mysql.php');
$e=array();
$sql="SELECT DISTINCT city from covid.records";
$da=exe_sql("covid",$sql);
$cases="";
$body="<div class='card-body'>";
$bodyClose="</div>";
$cardClose="</div>";
$header="";
$totals="";
$footer="";
foreach($da as $f=>$ci){
	$data[]=$ci[city];
}
sort($data);
//var_dump($data);
$all=array_search("All of Orange County",$data);

$allname=$data[$all];
if(isset($data[$all]))unset($data[$all]);
$data= array_values($data);
array_unshift($data,$allname);
foreach($data as $city){
$sql2="SELECT date,count from records where records.city=\"$city\"";
$d=exe_sql("covid",$sql2);
//var_dump($d);
//$d["city"]=$city['city'];


$cases.="";
$casect=count($d);
$showct=$casect-6;
$cityref=str_replace(" ","_",$city);
$cityref=str_replace("*","",$cityref);
$v3=0;
$cases .="<div id='{$cityref}_chart_div' style='margin:auto;'></div><u>Date</u>   -     <u>Total Cases</u><br/><div class='collapse multi-collapse' id='_{$cityref}'>";
$js="";
$js .="<script>google.charts.setOnLoadCallback(draw{$cityref});  function draw{$cityref}() {   var data = new google.visualization.DataTable(); data.addColumn('date', 'date'); data.addColumn('number', 'Count');data.addRows([ ";
foreach($d as $s=>$v){
    if(is_array($v)){
      $c3[]=$v['count'];
      $c2[]=$v['count'];
	  $dj=date("Y,m,d,0",strtotime($v['date']."-1 month"));
	$js .="[new Date($dj),{$v['count']}]";   if (($s+1) != $casect)$js.= ",";
     $cases.= date('F d',strtotime($v['date'])).' - <strong>'.$v['count'].'</strong> <br/>';
	   if($v3==$showct)$cases .="</div>";
 $v3++;
  //  $e['date'][]=$v['date'];
//$e['count'][]=$v['count'];
    }
  
}
$js .="]); var options = {title:'{$city}',     legend: 'none',               width:250, height:200};    var chart = new google.visualization.LineChart(document.getElementById('{$cityref}_chart_div')); chart.draw(data, options);  }</script>";
$cases .="  <a style='color:black;' data-toggle='collapse' href='#_{$cityref}' role='button' aria-expanded='false' aria-controls='{$cityref}'><span class='oi oi-expand-up'>click to expand</span></a> ";
$f=array_shift($c3);
$e=array_pop($c3);
$change=$e-$f;
unset($f);
unset($e);
unset($c3);
$e2=array_pop($c2);
$e3=array_pop($c2);
$lchange=$e2-$e3;
if($lchange>2){$not="danger"; $text="text-white";}else if($lchange===0){$not="success";  $text="text-white";}else if($lchange===1){$not="info";  $text="text-white";}else if($lchange===2){$not="warning";  $text="text-white";}
$header .= "<div class='card shadow m-2 bg-$not $text rounded text-center'><div class='card-header'><h4 class='text-wrap'>".$city."</h4></div>";
$totals.='<div class="alert alert-'.$not.'" role="alert"><strong>'.$lchange."</strong> New cases from yesterday</div>";
unset($e2);
unset($e3);
unset($c2);

$footer.="<div class='card-footer text-white'><strong>".$change."</strong> cases in $city since April 2 </div>";
 
unset($e);
 echo $header.''.$totals.''.$body.''.$js.''.$cases.''.$bodyClose.''.$footer.''.$cardClose;
$cases="";
$header="";
$totals="";
$footer="";
//$e["city"]=$city['city'];
//var_dump($e);
}
?>




</div>
</div>
 <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>